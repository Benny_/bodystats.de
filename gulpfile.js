/**
 * GULP TASKS
 * 'gulp <task name>'
 *
 * CLEAN
 * Clean the corresponding build folder.
 *
 * clean:css
 * clean:js
 * clean:images
 * clean:html
 * clean:all
 *
 *
 * COMPILE
 * Clean the build folder, compile/minify/uglify/optimize files and move them
 * to the build folder.
 *
 * compile:css
 * compile:js
 * compile:images
 * compile:html
 * compile:all
 *
 *
 * WATCH
 * Watch files for changes, clean the build folder and start the corresponding
 * 'compile' task.
 *
 * watch:css
 * watch:js
 * watch:images
 * watch:html
 * watch:all
 */


/**
 * GULP
 */
var gulp = require('gulp');


/**
 * GULP PLUGINS
 */
var autoprefixer = require('gulp-autoprefixer');
var bytediff     = require('gulp-bytediff');
var clean        = require('gulp-clean');
var include      = require('gulp-file-include');
var gutil        = require('gulp-util');
var imagemin     = require('gulp-imagemin');
var minifyCSS    = require('gulp-minify-css');
var notify       = require('gulp-notify');
var plumber      = require('gulp-plumber');
var rename       = require('gulp-rename');
var sass         = require('gulp-sass');
var uglify       = require('gulp-uglify');





/**
 * SETTINGS
 */
var path = {
    css: {
        src: 'src/assets/css',
        dest: 'dist/assets/css'
    },
    js: {
        src: 'src/assets/js',
        dest: 'dist/assets/js'
    },
    images: {
        src: 'src/assets/images',
        dest: 'dist/assets/images'
    },
    html: {
        src: 'src',
        dest: 'dist'
    }
};





/**
 * ERROR HANDLING
 * Thanks to Tim Roes (https://www.timroes.de/2015/01/06/proper-error-handling-in-gulp-js/)
 */
var gulp_src = gulp.src;

gulp.src = function() {
    return gulp_src.apply(gulp, arguments)
    .pipe(plumber(function(error) {
        // Output an error message
        gutil.log(gutil.colors.red('Error (' + error.plugin + '): ' + error.message));

        // emit the end event, to properly end the task
        this.emit('end');
    }));
};





/**
 * CLEAN
 * Cleans the corresponding build folder.
 */
gulp.task('clean:css', function() {
    return gulp.src(path.css.dest, {read: false})
    .pipe(clean());
});


gulp.task('clean:js', function() {
    return gulp.src(path.js.dest, {read: false})
    .pipe(clean());
});


gulp.task('clean:images', function() {
    return gulp.src(path.images.dest, {read: false})
    .pipe(clean());
});


gulp.task('clean:html', function() {
    return gulp.src(path.html.dest + '/*.html', {read: false})
    .pipe(clean());
});


gulp.task('clean:all', ['clean:css', 'clean:js', 'clean:images', 'clean:html']);





/**
 * COMPILE
 * Cleans the build folder, compile/minify/uglify/optimize files and move them
 * to the build folder.
 */
gulp.task('compile:css', ['clean:css'], function() {
    return gulp.src(path.css.src + '/**/*.scss')
    .pipe(sass())
    .pipe(bytediff.start())
    .pipe(minifyCSS({advanced: false}))
    .pipe(autoprefixer())
    .pipe(bytediff.stop())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(path.css.dest))
    .pipe(notify('CSS task complete'));
});


gulp.task('compile:js', ['clean:js'], function() {
    return gulp.src(path.js.src + '/**/*.js')
    .pipe(bytediff.start())
    .pipe(uglify())
    .pipe(bytediff.stop())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(path.js.dest))
    .pipe(notify('JS task complete'));
});


gulp.task('compile:images', ['clean:images'], function() {
    return gulp.src(path.images.src + '/*')
    .pipe(bytediff.start())
    .pipe(imagemin())
    .pipe(bytediff.stop())
    .pipe(gulp.dest(path.images.dest))
    .pipe(notify('JS task complete'));
});


gulp.task('compile:html', ['clean:html'], function() {
    return gulp.src(path.html.src + '/*.html')
    .pipe(include())
    .pipe(gulp.dest(path.html.dest))
    .pipe(notify('HTML task complete'));
});


gulp.task('compile:all', ['compile:css', 'compile:js', 'compile:images', 'compile:html']);





/**
 * WATCH
 * Watches files for changes, cleans the build directory and start the
 * corresponding 'compile' task.
 */
gulp.task('watch:css', function() {
    gulp.watch(path.css.src + '/**/*.scss', ['compile:css']);
});


gulp.task('watch:js', function() {
    gulp.watch(path.js.src + '/**/*.js', ['compile:js']);
});


gulp.task('watch:images', function() {
    gulp.watch(path.images.src + '/*', ['compile:images']);
});


gulp.task('watch:html', function() {
    gulp.watch(path.html.src + '/**/*.html', ['compile:html']);
});


gulp.task('watch:all', ['watch:css', 'watch:js', 'watch:images', 'watch:html']);
